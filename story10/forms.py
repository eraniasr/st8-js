from django import forms
from .models import User

class UserForm(forms.Form):
    email_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'yourname@gmail.com',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Full Name',
    }
    pass_attrs = {
        'type': 'password',
        'class': 'form-control',
        'placeholder':'Password',
    }
    name = forms.CharField(max_length = 50, error_messages={"required": "Please enter your name"}, widget = forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(error_messages={"required": "Please enter a valid email"}, max_length=50, widget=forms.TextInput(attrs=email_attrs))
    password = forms.CharField(max_length = 20, min_length=6, widget = forms.PasswordInput(attrs=pass_attrs), error_messages={"min_length": "password must be longer than 5 characters"})