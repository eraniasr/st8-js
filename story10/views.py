from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import User
from .forms import UserForm
# Create your views here.

def index(request):
    response = {}
    response["user"] = UserForm()
    response["user_list"] = User.objects.all()
    if request.method == 'POST':
        form_res = UserForm(request.POST)
        if form_res.is_valid():
            User.objects.create(user=request["user"])
        return HttpResponseRedirect("/story10/")
    return render(request, "Sign.html", response)

