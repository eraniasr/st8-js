from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.
class Story8UnitTest(TestCase):

    def test_page_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_uses_correct_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'Home.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)