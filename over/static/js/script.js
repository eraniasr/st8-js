$(document).ready(function() {
    var theme = false;

    $('.overlay').delay(4000);
    $('.overlay').slideUp(300);  
    
    $(".set > a").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this)
            .siblings(".content")
            .slideUp(200);
            $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
        } else {
            $(".set > a i")
            .removeClass("fa-minus")
            .addClass("fa-plus");
            $(this)
            .find("i")
            .removeClass("fa-plus")
            .addClass("fa-minus");
            $(".set > a").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp(200);
            $(this)
            .siblings(".content")
            .slideDown(200);
        }
    });

    $('#theme').on('click', function() {
        if(theme){
          $('body').css('background-color', '#f4e1d2');
          $('.set > a.active').css('background-color', '#f18973');
          $('#theme').css('background-color', '#bc5a45');
          $('#theme').css('border-color', '#c45138');
          theme = false;
        }else{
          $('body').css('background-color', '#f9d5e5');
          $('.set > a.active').css('background-color', '#eeac99');
          $('#theme').css('background-color', '#e06377');
          $('#theme').css('border-color', '#c83349');
          theme = true;
        };
        return false;
    });

});
